import * as React from 'react'
import * as Ui from 'material-ui'
import * as axios from 'axios'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Auth from "./Auth";

class Author extends React.Component {

	instance = null;
	
	constructor(props) {
 		super(props);
 		this.state = {
 			selectedAuthor: {
				id: '',
				name: ''
			},
 			authors: [],
 		};
 		this.instance = axios.create({
 			baseURL: 'http://localhost:8081/authors',
 			headers: {'x-access-token': Auth.getToken()}
 		});
	}

	componentDidMount = () => {
		this.refreshAuthorsList();
	}

	AuthorForm = (props) => {
 		let nameInput = <Ui.TextField floatingLabelText="Name" value={this.state.selectedAuthor.name} onChange={this.onAuthorNameChange}/>;
 		return (
			<form>
				<div>
					{nameInput}
				</div>
				<div>
					<Ui.FlatButton 
						label="New" 
						secondary={true} 
						onClick={this.handleNew}
						disabled={this.state.selectedAuthor.name === ''}
						icon={<Ui.FontIcon className="material-icons">create</Ui.FontIcon>} />
					<Ui.FlatButton 
						disabled={this.state.selectedAuthor.name === ''} 
						label="Save"
						primary={true} 
						onClick={this.handleSave} 
						icon={<Ui.FontIcon className="material-icons">save</Ui.FontIcon>} />
					<Ui.FlatButton 
						disabled={this.state.selectedAuthor.name === ''} 
						label="Remove"
						secondary={true} 
						onClick={this.handleRemove} 
						icon={<Ui.FontIcon className="material-icons">delete</Ui.FontIcon>} />
				</div>
			</form>
		)
};
	
	AuthorList = (props) => {
 		const authors = props.authors;
 		const listItems = authors.map((author) =>
 			<Ui.ListItem 
				key={author.id} 
				primaryText={author.name} 
				onClick={() => this.handleAuthorClick(author)}/>
 		);
 		return (
 			<Ui.List>{listItems} </Ui.List>
 		);
	};

	onAuthorNameChange = (event) => {
 		let author = this.state.selectedAuthor;
 		author.name = event.target.value;
 		this.setState({
 			selectedAuthor: author
 		});
	};	

	handleAuthorClick = (author) => {
 		this.setState({selectedAuthor: author});
	};

	handleSave = () => {
		let id = this.state.selectedAuthor.id;
 		this.instance.put('/' + id, {
 			name: this.state.selectedAuthor.name
 		}).then(response => {
 			this.refreshAuthorsList();
 		})
	};
	
	handleNew = () => {
 		this.instance.post('/', {
 			name: this.state.selectedAuthor.name
		}).then(response => {
 			this.refreshAuthorsList();
		})
	};
	
	handleRemove = () => {
 		let id = this.state.selectedAuthor.id;
 		this.instance.delete("/" + id).then(response => {
 			this.refreshAuthorsList();
 			this.setState({selectedAuthor : {
 				id: '',
 				name: ''
 			}})
 		})
	};

	refreshAuthorsList = function() {
		this.instance.get('/')
			.then(response => {
				let authors = response.data;
				this.setState({
					authors
				});
			})
			.catch(error => {
				if (error.response.status === 401)
					this.props.history.push('/login');
			});
	};

	render(){
 		return (
 			<MuiThemeProvider>
 				<div>
 					<h3>Authors</h3>
 					<Ui.Paper zDepth={1} className="left-column">
 						<this.AuthorList authors={this.state.authors}/>
 					</Ui.Paper>
 					<Ui.Paper zDepth={1} className="right-column">
 						<this.AuthorForm/>
 					</Ui.Paper>
 				</div>
 			</MuiThemeProvider>
 		)
	}

}
export default Author;