import * as React from 'react'
import * as Ui from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Auth from "./Auth";
import * as axios from "axios";

class Login extends React.Component {

	style = {
 		height: 250,
 		width: 300,
 		margin: '200px auto auto auto',
 		display: 'block',
 		padding: '20px 20px 20px 20px'
	};

	constructor(props) {
 		super(props);
		this.state = {
 			error : '',
 			user: {
 				_username: '',
 				_password: ''
 			}
		};
		this.instance = axios.create({
 			baseURL: 'http://localhost:8081/users/login',
 			headers: {'Content-type': 'application/json'}
 		});
 	}
	
	onUsernameChange = (event) => {
		let user = this.state.user;
 		user._username = event.target.value;
 		this.setState({
 			user: user
 		});
	};

	onPasswordChange = (event) => {
 		let user = this.state.user;
 		user._password = event.target.value;
 		this.setState({
 			user: user
 		});
	};

	handleLogin = (event) => {
		let user = this.state.user;
		this.instance.post("/", {
			username: user._username,
			password: user._password
		}).then(response => {
 			if (response.status === 200) {
 				this.setState({
 					errors: {}
 				});
 				Auth.authenticateUser(response.data.token);
 				this.props.history.push('/authors');
			} else {
				const error = response.data.message ? response.data.message : {};
				this.setState({
 					error
				});
 			}
		});
		event.preventDefault();
	};

	render() {
 		return (
 			<MuiThemeProvider>
 				<form onSubmit={this.handleLogin}>
 					<Ui.Paper zDepth={3} style={this.style}>
						<div>{this.state.error}</div>
 						<div>
							<Ui.TextField floatingLabelText="Username" onChange={this.onUsernameChange} />
 						</div>
 						<div>
							<Ui.TextField floatingLabelText="Password" onChange={this.onPasswordChange} type="password" />
						</div>
 						<div> 
							<Ui.RaisedButton type="submit" label="Login" primary={true} />
						</div>
 					</Ui.Paper>
 				</form>
 			</MuiThemeProvider>
 		);
	}

}
export default Login;