import * as React from 'react'
import * as Ui from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import './App.css';
import {Link} from "react-router-dom";

class Navigation extends React.Component {

	constructor(props) {
 		super(props);
 		this.state = {
 			open: false,
 		};
	}
	
	render() {
		return (
			<MuiThemeProvider>
				<div>
					<Ui.AppBar
						title="Book Store"
						iconClassNameRight="muidocs-icon-navigation-expand-more"
						onLeftIconButtonTouchTap={this.openNavigationDrawer}
					/>
					<Ui.Drawer open={this.state.open} docked={false} onRequestChange={(open) => this.setState({open})}>
						<Ui.MenuItem 
							leftIcon={<Ui.FontIcon className="material-icons">face</Ui.FontIcon>} 
							primaryText="Authors" 
							containerElement={<Link to="/authors"/>}>
						</Ui.MenuItem>
						<Ui.MenuItem
							leftIcon={<Ui.FontIcon className="material-icons">book</Ui.FontIcon>}
							primaryText="Books" 
							containerElement={<Link to="/books"/>}>
						</Ui.MenuItem>
						<Ui.MenuItem 
							leftIcon={<Ui.FontIcon className="material-icons">exit_to_app</Ui.FontIcon>}
							primaryText="Login" 
							containerElement={<Link to="/login"/>}>
						</Ui.MenuItem>
					</Ui.Drawer>
				</div>
			</MuiThemeProvider>
 		);
	}
	
	handleRequestClose = () => {
 		this.setState({
 			open: false,
 		});
	};

	openNavigationDrawer = (event) => {
 		this.setState({
 			open: true,
 		});
	};

}
export default Navigation;